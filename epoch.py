from numpy import datetime64, timedelta64

def epoch2utc(tsf):
	"""epoch2utc(bytestring: timestamp)
	Converts epoch time stamp to UTC time
	
	timestamp is a float like byte string e.g. 1700776446.140
	returns a numpy datetime64 object

	example of common use
	np.genfromtxt("file.csv", converters={0:epoch2utc})

	"""
	tsf = tsf.decode('utf-8')
	ts, f = tsf.split('.')
	ts = int(ts)
	f = int(f)

	us = timedelta64(f, 'us')
	utcs = datetime64(ts, 's')

	utc = utcs + us
	return utc




